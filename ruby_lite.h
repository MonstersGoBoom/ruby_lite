//	include this for implementation 
//
//	Minimal Ruby-Lite 
//	using the system DLL with LoadLibrary / GetProcAddress
//	we grab the bare minimal ruby functions to work 
//	there is no need for Ruby.h or Ruby to be installed ( just needs the DLL files in the path ) 

/*
//	IMPLEMENTATION IS HERE
#include "ruby_lite.h"

static VALUE rb_samplefunc(VALUE self,VALUE str)
{
	printf("rb_samplefunc got %s",StringValueCStr(str));
	return Qnil;
}

int main(int argc,char *argv)
{
	if (ruby_lite()==true)
	{
		int state;  
		VALUE result;

		ruby_init();
		ruby_init_loadpath();
		rb_define_global_function("samplefunc",rb_samplefunc, 1);
		rb_define_global_const("CONSTANT",INT2NUM(12));
		ruby_script("main");

		result = rb_eval_string_protect("samplefunc(\"Hello World\")", &state);
		if (state)
		{
			VALUE errorLineNumber = rb_gv_get("$errorLineNumber");    
			VALUE errorString = rb_inspect(rb_gv_get("$!"));
			printf( "ruby eval failed : %d\n", state);
			printf( "ruby $! : %s", StringValueCStr(errorString));
			exit(0);
		}
	}
}
*/

#define Qfalse 0x00
#define Qtrue  0x14
#define Qnil   0x08
#define ANYARGS

typedef uintptr_t VALUE;
typedef intptr_t SIGNED_VALUE;

#define ruby_init 								_ruby_init
#define ruby_init_loadpath 				_ruby_init_loadpath
#define rb_define_global_function _rb_define_global_function
#define rb_define_global_const 		_rb_define_global_const
#define rb_define_module_function _rb_define_module_function
#define ruby_script 							_ruby_script
#define rb_define_module 					_rb_define_module
#define rb_define_method 					_rb_define_method
#define rb_num2dbl 								_rb_num2dbl
#define rb_string_value_cstr 			_rb_string_value_cstr
#define rb_num2long 							_rb_num2long
#define rb_gv_set 								_rb_gv_set
#define rb_gv_get 								_rb_gv_get
#define rb_ary_new_from_args 			_rb_ary_new_from_args
#define rb_eval_string_protect		_rb_eval_string_protect
#define rb_inspect								_rb_inspect

#define rb_int_new(v) 						_rb_int2inum(v)
#define RB_NUM2LONG(x) 						rb_num2long(x)
#define NUM2DBL(x) 								rb_num2dbl((VALUE)(x))
#define NUM2INT(x) 								_rb_num2int(x)
#define INT2NUM(x) 								_rb_int2big(x)
#define StringValueCStr(v) 				rb_string_value_cstr(&(v))

#define RB_LIST \
    /* ret, name, params */ \
		RBE(void,ruby_init,void) \
		RBE(void,ruby_init_loadpath,void) \
		RBE(void,ruby_script,const char*) \
		RBE(void,rb_define_global_function,const char*,VALUE(*)(ANYARGS),int) \
		RBE(void,rb_define_method,VALUE,const char*,VALUE(*)(ANYARGS),int) \
		RBE(void,rb_define_module_function,VALUE,const char*,VALUE(*)(ANYARGS),int) \
		RBE(VALUE,rb_eval_string_protect,const char*,int *) \
		RBE(VALUE,rb_gv_get,const char*) \
		RBE(VALUE,rb_inspect,VALUE) \
		RBE(double,rb_num2dbl,VALUE) \
		RBE(VALUE,rb_int2big,SIGNED_VALUE) \
		RBE(char *,rb_string_value_cstr,volatile VALUE*) \
		RBE(long,rb_num2long,VALUE) \
		RBE(int,rb_num2int,VALUE) \
		RBE(void,rb_define_const,VALUE,const char*,VALUE) \
		RBE(void,rb_define_global_const,const char*,VALUE) \
		RBE(VALUE,rb_define_module,const char*) \
		RBE(VALUE,rb_ary_new_from_args,long n, ...) \
		RBE(VALUE,rb_int2inum,intptr_t) \

//	declare typedefs

#define RBE(ret, name, ...) typedef ret name##proc(__VA_ARGS__); // extern name##proc * _##name;
RB_LIST
#undef RBE

//	declare functions

#define RBE(ret, name, ...) name##proc * _##name;
RB_LIST
#undef RBE

//	load the lib and get the addresses

bool ruby_lite()
{
#if defined(__linux__)
	void* libRuby = dlopen("libruby-2.3.so.2.3", RTLD_LAZY);
	if (!libRuby)
	{
		printf("ERROR: libRuby.so couldn't be loaded\n");
		return false;
	}
  #define RBE(ret, name, ...)                                                    \
					_##name = (name##proc *)dlsym(libRuby, #name);\
  				if (!_##name) {																					\
						printf("Function " #name " couldn't be loaded\n");		\
						return false;																					\
					} 
	      RB_LIST
  #undef RBE
	return true;
#else
  printf("Checking Ruby 2.5.0 is installed\n");
	HINSTANCE ruby_dll = LoadLibraryA("x64-msvcrt-ruby250.dll");
	if (ruby_dll==NULL)
	{
  	printf("Checking Ruby 2.4.0 is installed\n");
		ruby_dll = LoadLibraryA("x64-msvcrt-ruby240.dll");
	}
	if (ruby_dll==NULL)
	{
	  printf("Ruby isn't installed or available\n");
		return false;
	}
	#define RBE(ret, name, ...)																			\
					_##name = (name##proc *)GetProcAddress(ruby_dll, #name);\
					if (!_##name) {																					\
						printf("Function " #name " couldn't be loaded\n");		\
						return false;																					\
					} 
			RB_LIST
	#undef RBE

	return true;
#endif
	return false;	
}
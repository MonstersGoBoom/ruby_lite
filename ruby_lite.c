#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#if defined(__linux__)
	#include <dlfcn.h>
#else
	#include <windows.h>
#endif
//	IMPLEMENTATION IS HERE
#include "ruby_lite.h"

static VALUE rb_samplefunc(VALUE self,VALUE str)
{
	printf("rb_samplefunc got %s",StringValueCStr(str));
	return Qnil;
}

int main(int argc,char *argv)
{
	if (ruby_lite()==true)
	{
		int state;  
		VALUE result;

		ruby_init();
		ruby_init_loadpath();
		rb_define_global_function("samplefunc",rb_samplefunc, 1);
		rb_define_global_const("CONSTANT",INT2NUM(12));
		ruby_script("main");

		result = rb_eval_string_protect("samplefunc(\"Hello World\")", &state);
		if (state)
		{
			VALUE errorLineNumber = rb_gv_get("$errorLineNumber");    
			VALUE errorString = rb_inspect(rb_gv_get("$!"));
			printf( "ruby eval failed : %d\n", state);
			printf( "ruby $! : %s", StringValueCStr(errorString));
			exit(0);
		}
	}
}
